%rebase('editbase.tpl')

        <!-- contact -->
        <section id="contact">
            <div class="inner">
            <h2 class="section-title">お知らせ一覧管理画面</h2>
            
            <!-- contact-form -->
            <form class="contact-form" action="/editnews" method="POST">
            
            <dl class="contact-dl">
                <dt class="contact-label">日付</dt>
                <dd class="contact-input"><input type="text" value="" name="datetime" placeholder="2019-01-30"></dd>
            
                <dt class="contact-label">お知らせ種別</dt>
                <dd class="contact-input">
                    <select id="contact-select" name="newInfo">
                        <option value="ーーー" class="gray">ーーー</option>
                        <option value="新着" class="red">新着</option>
                        <option value="案内" class="blue">案内</option>
                    </select>
                </dd>
            
                <dt class="contact-label">お知らせ内容</dt>
                <dd class="contact-input"><textarea name="infoContents" placeholder="お知らせ内容を入力してください"></textarea></dd>
            </dl>
            
            <div class="contact-send">
                <input type="submit" name="save" value="追加する">
            </div><!-- /contact-send -->
            
    </form><!-- /contact-form -->
<div class="newslist">
    %for item in news_info:
    <ul>
        <!-- <li>{{item["id"]}}</li> -->
        <li>{{item["datetime"]}}</li>
        <li>{{item["newInfo"]}}</li>
        <li>{{item["infoContents"]}}</li>
        <li><a href="/del/{{item['id']}}" class="del-btn">✖</a></li>
    </ul>
    %end
</div>
    
            </div><!-- /inner -->
            </section><!-- /contact -->
    