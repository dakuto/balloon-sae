import os
import sqlite3
from bottle import route , run , template , TEMPLATE_PATH , request , redirect ,error, response ,static_file

# ファイル名が変わったら下記のルーティング変更
TEMPLATE_PATH.insert(0, os.path.abspath(
    os.path.join(os.path.dirname("__file"),"../union_portfolio")))



# ここからメイン画面のルーティング


@route('/index')
def index():
    # balloon.dbに接続
    conn = sqlite3.connect('portfolio.db')
    c = conn.cursor()



      # ここから紹介文のルーティング。SQLから紹介文の本文を選ぶ
    c.execute("select body from contents  where sec =1")
    # 選んだ本文を取得してintro_oldに代入する
    intro = c.fetchone()
    print(intro)


    # ここからお知らせのルーティング
    c.execute("select datetime,newInfo,infoContents from info order by id desc")
    
    news_info = []
    print(news_info)

    for row in c.fetchall():
        news_info.append({"datetime":row[0],"newInfo":row[1],"infoContents":row[2]})

    print(news_info)


    # portfolio.dbのGALLERY テーブルからimgのfile名を取ってくる
    c.execute("select img from GALLERY")

    gallery_img = c.fetchall()
    print(gallery_img)

    # データベース接続終了
    c.close()
    return template('index',intro=intro,news_info = news_info,gallery_img=gallery_img)




# # ここまでメイン画面のルーティング





# # ここから編集画面のルーティング
# 編集メニューメイン画面
@route("/editmain")
def editmain():
    return template("editmain")


# 紹介文編集画面
@route("/editintro")
def intro():

    intro = ["hello"]

    return template("editintro" , intro_old = intro)

# お知らせ編集画面

# お知らせ編集画面表示のGetmethod
@route('/editnews', method=["GET"])
def editnews_get():
    conn = sqlite3.connect('portfolio.db')
    c = conn.cursor()

    # sql文を発行する
    c.execute("select id,datetime,newInfo,infoContents from info")
    
    news_info = []

    for row in c.fetchall():
        news_info.append({"id":row[0],"datetime":row[1],"newInfo":row[2],"infoContents":row[3]})

    print(news_info)

    # データベース接続終了
    c.close()

    return template('editnews', news_info = news_info)

# ここで入力されたデータを取得し保存する作業
@route('/editnews', method=["POST"])
def editnews_post():

    datetime = request.POST.getunicode('datetime')
    newInfo = request.POST.getunicode('newInfo')
    infoContents = request.POST.getunicode('infoContents')

    conn = sqlite3.connect('portfolio.db')

    c = conn.cursor()

    c.execute("insert into info values(null,?,?,?);" , ( datetime, newInfo, infoContents))

    conn.commit()

    conn.close()

    return redirect('/editnews')

# 削除するための履歴表示画面
@route('/info_list')
def newsinfo():
    # balloon.dbに接続
    conn = sqlite3.connect('portfolio.db')
    c = conn.cursor()

    # sql文を発行する
    c.execute("select id,datetime,infoContents from info")
    
    news_info = []

    for row in c.fetchall():
        news_info.append({"id":row[0],"datetime":row[1],"infoContents":row[2]})

    print(news_info)

    # データベース接続終了
    c.close()
    return template('info_list', news_info = news_info)


# 削除の処理がここ
@route('/del/<id:int>')
def del_info(id):
	conn=sqlite3.connect('portfolio.db')
	c=conn.cursor()
	c.execute('delete from info where id =?',(id,))
	conn.commit()
	conn.close()
	return redirect('/editnews')


# GALLERY編集画面
@route('/editgallery',method=["GET"])
def editgallery():
    conn = sqlite3.connect("portfolio.db")
    c = conn.cursor()

    c.execute("select img from GALLERY")
    
    editgallery = c.fetchall()
    print(editgallery)
    # 終了
    conn.close()

    return template("editgallery",editgallery=editgallery)



@route('/editgallery',method=["POST"])
def editgallery_img():
    # editgallery_old(HTMLのフォームから入力された値)を変数oldに代入する
    # upload = request.files.get('edit_img', '')
    pload_files = request.files.getall('upload[image_file_name][]')
    print("upladeの中身は")
   
    # if not upload.filename.lower().endswith(('.png','.jpg','.jpeg')):
        # return 'png,jpg,jpeg形式のファイルを選択してください'
    
    save_path = get_save_path()
    file = "test"
    id = 1
    for count,uplad_file in enumerate(pload_files):
        upload_img = uplad_file
        if not upload_img.filename.lower().endswith(('.png','.jpg','.jpeg')):
            return template("no {{uplad_file}}", uplad_file=uplad_file)
        print("中身")
        # save_path = get_save_path() 
        try:
            save_path = get_save_path() 
            upload_img.save(save_path)
        except:
            upload_img = uplad_file
        print(upload_img.filename)
        file = upload_img.filename
        print(file)
        print("ファイル名取れていますか？")
        conn = sqlite3.connect("portfolio.db")
        c = conn.cursor()
        # ここでSQLに新しい値を更新している。？がフォームに入力された値に更新される（introは↑で設定したHTMLのフォームから入力された値）
        c.execute("update GALLERY set img = ? where id = ?", (file ,id))
        # 保存→終了
        conn.commit()
        id = id + 1
    conn.close()
# ※※returnがないとpythonから値が返ってこないので気を付けて※※
    return redirect("/editgallery")

def get_save_path():
    path_dir = "./static/photos"
    return path_dir



# /static/<filepath:path>←staticはスタイルシートとかjavascriptのシートがここにありますよ～っていう目印みたいなフォルダ
# このフォルダを見つけられなくて2時間かかったからここのルーティングも気をつけなはれや
# ※※このstatic_fileはimportに入れないといけない※※
# ※※CSS、ｊｓ、imageフォルダはstaticにまとめないと適用されないから必ずstaticフォルダ作ろう！
@route("/static/<filepath:path>")
def server_static(filepath):
    return static_file(filepath ,root="static")

# run関数は必ずプログラムの末尾につける。
# run(port="8010",reloader = True)

run(host="0.0.0.0", port=int(os.environ.get("PORT", 5000)))