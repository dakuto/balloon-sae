<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="static/css/style.css">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/lity/1.6.6/lity.css' />  

    <title>ふうせん屋Sae</title>
</head>


<body> 
    <!-- header -->
    <header>
        <div id="header">
            <div class="inner">
        <!-- header-nav -->
                <nav class="header-nav">
                    <ul class="header-list">
                        <li class="header-item"><a href="#">TOP</a></li>
                        <li class="header-item"><a href="#decoration">出演依頼</a></li>
                        <li class="header-item"><a href="#school">バルーン教室</a></li>
                        <li class="header-item"><a href="#news">新着情報</a></li>
                        <li class="header-item"><a href="#contact">問い合わせ</a></li>
                    </ul>             
                </nav><!-- /header-nav -->
            <a href="#" class="header-item"><img src="static/F_Logo_Online_09_2018/Color/PNG/f-ogo_RGB_HEX-58.png"></a>
            </div><!-- /inner -->
         </div>
        </header><!-- /header -->
        

       <!-- mvwrapper -->
        <div id="mvwrapper">
            <!-- mv -->
            <div id="mv"> 
            <div class="inner">
                <div class="mv-mask">
                    <!-- mv-content -->
                    <div class="mv-content" >
                    <div class="mv-title"><img src="/static/image/ふうせん屋ヘッダーロゴ01.png" alt="ふうせん屋 Sae"></div><!-- /mv-title -->
                        
                    </div><!-- /mv-content -->
                </div>
             </div><!-- /inner -->
        </div><!-- /mv -->
    </div> <!-- mvwrapper -->
        
    

        <!-- introduction -->
        <section id="introduction">
        <div class="inner">
        <h2 class="introduction-section-title">紹介文</h2>
        

            <p>ふうせん屋Saeとは<br><br>
                気軽に楽しく新しいことを始めたい<br>
                 バルーンアートに興味がある<br>
                 子供と一緒に遊べる趣味が欲しい<br><br>
                 
                 そんな方々向けのバルーンアート講座とイベントでの出張ライブパフォーマンスを行っております。<br><br>
                 
                 バルーンアートを楽しみたいお客様からのご連絡を心よりお待ちしております！
            </p>
        
        
        </div><!-- /inner -->
        </section><!-- /introduction -->

    <!--           バナーボタン         -->
    <div class="box6">
        <div class="inner">
        <div class="box6-1">
            <div class="gaiyou">
            <a href="#school">
            <div class="box6-img01">
                <img src="/static/image/117451b.png" alt="">
            </div>
            <h1>バルーン教室について</h1>
            <p>月に一度、各地域でバルーン教室を<br>
            和気あいあいと開いています。</p>
            </a>
            </div>
        </div>
        
        <div class="box6-2">
            <div class="gaiyou">
            <a href="#decoration">
            <div class="box6-img02">
                <img src="/static/image/138397r.png" alt="">
            </div>
            <h1>装飾・ステージの依頼</h1>
            <p>イベントに合わせたコンセプトで<br>
            バルーンでデコレーションしちゃいます！</p>
            </a>
            </div>
        </div>
        
        <div class="box6-3">
            <div class="gaiyou">
            <a href="#gallery">
            <div class="box6-img03">
                <img src="/static/image/139896g.png" alt="">
            </div>
            <h1>ギャラリー</h1>
            <p>教室やイベントの様子を掲載してるので<br>
            ぜひぜひご覧くださ～い！</p>
            </a>
            </div>
        </div>
        </div>
    </div>


<!-- info_list.tpl   お知らせの表示 -->
 <!-- news -->
 <section id="news">
    <div class="inner">
    <h2 class="section-title">お知らせ</h2>
    
    <!-- news-items -->
    <div class="news-items">
    %for item in news_info:
    
    <!-- news-item -->
    <div class="news-item">
        <div class="news-meta">
            <time class="news-time">{{item["datetime"]}}</time><!-- /news-time -->
    %if item["newInfo"] == "新着":
    <div class="news-tag m_tag1 red"><a href="">{{item["newInfo"]}}</a></div><!-- news-tag -->
    %elif item["newInfo"] == "案内":
    <div class="news-tag m_tag1 blue"><a href="">{{item["newInfo"]}}</a></div><!-- news-tag -->
    %else:
    <div class="news-tag m_tag1 gray"><a href="">{{item["newInfo"]}}</a></div><!-- news-tag -->
    %end

        </div><!-- /news-item-meta -->
        <div class="news-title"><a href="">{{item["infoContents"]}}</a></div><!-- /news-title -->
    </div><!-- /news-item -->
    %end
    </div><!-- /news-items -->
    
    
    </div><!-- /inner -->
    </section><!-- /news -->


<!-- ↑info_list.tpl   お知らせの表示ここまで -->
<!-- バルーン教室　ここから -->
<section id="school">
    <div class="inner port">
        <h2 class="section-title">バルーン教室について</h2>
        <div class="school-image">
            <img src="/static/image/マーメイド.jpg" alt="バルーン教室の様子">
            <div class="parent">
                <p class="child">
                        「バルーンって気になるけど、どんな感じなんだろう？」、「なにかに夢中になりたいな」
                        と思ったら、ご家族、お友達を誘って一緒にバルーン教室で楽しみながら作りましょう。<br>
                        「行きたいけど、日程が合わない」、「会場が遠いので、地元に来て欲しい」そんなご希望があれば、お気軽にお問合せ下さい。<br>
                        出張スクールも承っております。<br>
                        <span>費用：2000円～<br>
                        準備する物：ポンプ、はさみ、黒マジック<br>
                        ※お持ちでない場合はこちらでもご準備してます。</span>
                </p>
            </div>
        </div>
    </div>
</section>
<!-- バルーン教室　ここまで -->


<!-- event -->
<section id="decoration">
        <div class="inner port">
            <h2 class="section-title">装飾・ステージの依頼について</h2>
            <div class="decoration-image">
                <img src="/static/image/bitmap01.png" alt="イベントの様子">
                <div class="parent">
                    <p class="child">
                        「自治会や学校・幼稚園、小児病棟向けに、ステージをしてほしい」「イベント会場で、お客様参加型の教室をひらきたい」「行きたいけど、日程が合わない」
                        「会場が遠いので、地元に来て欲しい」そんなご希望があれば、お気軽にお問合せ下さい。<br>
                        お客様のご希望やお予算に合わせて、ステージの依頼、出張スクールも承っております。 
                        <a href="#contact" class="event-button">出演依頼</a>
                    </p>

                </div>
              
            </div>

        </div>
    </section>


        </div><!-- /inner -->
        </section><!-- event -->


        <!-- ギャラリー -->
     <!-- Pythonで画像のルーティングを取ってきています -->
     <div class= "inner">
     <h2 id ="gallery" class="section-title">GALLERY</h2>
     <div class="gallery_container">

    %for img in gallery_img: 
    <img class="gallery_display" src="/static/image/{{img[0]}}">
    %end
    </div>
</div>
</div>
<!-- <a href="photos/名称未設定-8124.jpg" data-lity="data-lity">  
        <img src='photos/名称未設定-8124.jpg'>
      </a> 

      <a href="photos/名称未設定-8119.jpg" data-lity="data-lity">  
        <img src='photos/名称未設定-8119.jpg'>
      </a>  -->
  
    <!-- contact -->
    <section id="contact">
        <div class="inner">
        
        <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSexesGyiaCcbyh68-D3buNl6EZZutYd9FkUkPq8wJVjXYlzRg/viewform?embedded=true" width="100%" height="981" frameborder="0" marginheight="0" marginwidth="0">読み込んでいます...</iframe>
        
        
        <!-- facebook埋め込み --------------------->
        <section id="facebook"></section>
        <div class="facebook-like-box inner">
        <iframe  src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2F%25E3%2582%25B8%25E3%2583%25A5%25E3%2582%25A8%25E3%2583%25BC%25E3%2583%25AB-1473295756138557%2F%3Fmodal%3Dadmin_todo_tour&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=186277552048993" style="border:none;overflow:hidden" height="500" width="100%" scrolling="no" frameborder="0" allow="encrypted-media"></iframe>
        </div>
        
        <!-- ここまでフェイスブック埋め込み -->
        
            <!-- bottom navigation -->
    
    <!-- フッター固定スマホバージョンの時 嘉手納 -->
    
    <ul class="bottom-menu">
        <li>
        <!--　↓↓項目1. ホーム 　＃の部分にホームのURLを入れる -->
            <a href="#mv">
        <i class="blogicon-home"></i><br><span class="mini-text">ホーム</span></a>
        </li>
    
        <li class="menu-width-max">
        
            <a href="#news"><i class="blogicon-list"></i><br><span class="mini-text">お知らせ</span></a>
        </li>
        <li>
        
         <a href="#contact">
        <i class="blogicon-hatenablog"></i><br><span class="mini-text">メール</span></a>
        </li>
        <li>
        
            <a href="#facebook">
        <i class="blogicon-twitter"></i><br><span class="mini-text">facebook</span></a>
        </li>
        
    </ul>
    
    <!-- ここまでフッター -->
    <script type="text/javascript" src="/js/jquery-3.2.1.min.js"></script>
    <script src="js/lightbox.js"></script>
    <script src="/js/lightbox.min.js"></script>
    <script>
        lightbox.option({
        /*オプションの記述の例*/
        'wrapAround': true,
        'alwaysShowNavOnTouchDevices': true
        })
        </script>
        
    <!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/lightbox.js" type="text/javascript"></script> -->
        </body>
    </html>